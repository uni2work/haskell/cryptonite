{
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "master";
    };
    flake-utils = {
      type = "github";
      owner = "numtide";
      repo = "flake-utils";
      ref = "master";
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem
    (system:
      let pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
      in {
        devShell = pkgs.mkShell {
          name = "uni2work-cryptonite";
          nativeBuildInputs = with pkgs.haskellPackages; [ stack ];
        };
      }
    );
}
